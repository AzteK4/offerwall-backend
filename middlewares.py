import time
import json
import logging
import falcon
import requests
import hashlib


class LoggingMiddleware:
    def __init__(self, debug_mode=True):
        self.logger = logging.getLogger('LoggingMiddleware')

    def process_request(self, req, resp):
        req.context.start = time.time()
        self.logger.debug("Requesting %s %s" % (req.method, req.url))

    def process_response(self, req, resp, resource, req_succeeded):
        if 'start' not in req.context:
            return

        duration = time.time() - req.context.start
        message = f"{req.method} {req.uri} - {resp.status} [{duration:.3f}s]"

        if int(resp.status.split()[0]) < 400:
            self.logger.info(message)
        else:
            self.logger.error(message)


class AuthMiddleware():

    GOOGLE_CLIENT_ID = "947102302627-0rhq0v8bcl1rsucj36prj8rbkq13linn.apps.googleusercontent.com"
    VALIDATE_URL = "https://oauth2.googleapis.com/tokeninfo"
    TOKEN_PREFIX = "access_token"

    def __init__(self, cache, exempt_routes=[], exempt_methods=['OPTIONS']):
        self.logger = logging.getLogger('AuthMiddleware')
        self.cache = cache
        self.exempt_routes = exempt_routes
        self.exempt_methods = exempt_methods

    def token_key(self, token):
        return f"{self.TOKEN_PREFIX}:{token}"

    def get_token(self, req):
        token = req.auth
        if not token:
            self.logger.error(
                'Request was cancelled because of missing Authorization header')
            raise falcon.HTTPUnauthorized(description="Missing Authorization header")

        if token.startswith('Bearer'):
            token = token[7:]
        self.logger.debug(f'Token - "{token}"')

        return token

    def process_request(self, req, resp):
        if req.path in self.exempt_routes or req.method in self.exempt_methods:
            return

        token = self.get_token(req)
        token_md5 = hashlib.md5()
        token_md5.update(token.encode())

        cache_key = self.token_key(token_md5.hexdigest())
        self.logger.debug(f'Cache key - {cache_key}')

        if self.cache.get(cache_key) is not None:
            # Token is valid and not expired
            self.logger.info('Token was found in cache')
            return

        self.logger.debug("Asking Google API to validate token")
        r = requests.get(self.VALIDATE_URL, params={
            'id_token': token
        })
        if r.status_code == 200:
            jwt_token = r.json()
            if jwt_token['aud'] != self.GOOGLE_CLIENT_ID:
                raise falcon.HTTPUnauthorized(
                    "Received token belongs to different service")

            self.logger.debug(
                f"Google Oauth token - {json.dumps(jwt_token, indent=4)}")
            b = self.cache.set(cache_key, jwt_token)
            self.logger.info(f'Setting in cache result - {b}')
        else:
            raise falcon.HTTPUnauthorized("Invalid Token")


class CORSMiddleware():
    def process_request(self, req, resp):
        if req.method == 'OPTIONS':
            raise falcon.HTTPStatus(falcon.HTTP_OK)

    def process_response(self, req, resp, resource, req_succeeded):
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header('Access-Control-Allow-Methods',
                        'GET, HEAD, POST, OPTIONS')
        resp.set_header('Access-Control-Allow-Headers',
                        'Authorization,Content-Type')
