import json
import falcon
import logging

from datetime import datetime, timedelta

class StatsResource:

    CACHE_TTL = 300
    DATE_PATTERN = '%Y-%m-%d'
    QUERY_FILTERS = ['advertiser', 'publisher', 'offer']
    DEFAULT_FILTERS = {'status': 'confirmed'}

    def __init__(self, mongo_client, cache):
        self.mongo = mongo_client
        self.cache = cache
        self.logger = logging.getLogger('StatsResource')

    def on_get_overcaps(self, req, resp):
        result = {}

        if (r := self.cache.get('stats:overcaps')) is not None:
            result = r
        else:
            result = self._get_mongo_overcaps()
            self.cache.set('stats:overcaps', result, expires_in=self.CACHE_TTL*10)

        resp.media = {'data': result}
        resp.status = falcon.HTTP_200

    def on_get_groupby(self, req, resp):
        date_from = self._validate_date_param(req, 'filter[date_from]')
        date_to = self._validate_date_param(req, 'filter[date_to]')

        group_param = req.params.get('group_by')
        if group_param is None:
            raise falcon.HTTPBadRequest(description="Empty 'group_by' entity")
        if group_param == 'app_id':
            group_param = 'offer.app_id'

        limit = req.get_param_as_int('limit')
        filters = self._get_filters_from_query(req)

        resp.media = {'data': self._proceed_period(
            (date_from, date_to), [group_param, ], filters=filters, limit=limit)}
        resp.status = falcon.HTTP_200

    def on_get_comparison(self, req, resp):
        p = []

        date_from = self._validate_date_param(req, 'first[from]')
        date_to = self._validate_date_param(req, 'first[to]')
        p.append((date_from, date_to))

        date_from = self._validate_date_param(req, 'second[from]')
        date_to = self._validate_date_param(req, 'second[to]')
        p.append((date_from, date_to))

        group_by = [req.params.get('group_by'), ]
        filters = self._get_filters_from_query(req)
        limit = self._get_param_as_int(req, 'limit')

        resp.media = {'data': {
            'first': self._proceed_period(p[0], group_by, filters=filters, limit=limit),
            'second': self._proceed_period(p[1], group_by, filters=filters, limit=limit),
        }}
        resp.status = falcon.HTTP_200

    def on_get_periods(self, req, resp):
        # `group_by` param can be both String and []String
        # If array, order reflects hierarchy of offers' grouping
        group_by = req.params.get('group_by') or req.params.get('group_by[]')
        if group_by is None:
            raise falcon.HTTPBadRequest(description="Error in 'group_by'")
        if isinstance(group_by, str):
            group_by = [group_by, ]
        self.logger.info(f'{group_by=}')

        filters = self._get_filters_from_query(req)
        limit = self._get_param_as_int(req, 'limit')

        # Defining periods when to check stats
        today = datetime.today()
        week_start = today - timedelta(days=today.weekday())
        month_start = today.replace(day=1)
        prev_month = (month_start - timedelta(days=1)).replace(day=1)

        periods = [
            ('yesterday', today-timedelta(days=1)),
            ('day_before', today-timedelta(days=2)),
            ('this_week', week_start, today),
            ('last_week', week_start-timedelta(weeks=1), week_start),
            ('this_month', month_start, today),
            ('last_month', prev_month, month_start-timedelta(days=1)),
        ]

        # TODO: Parallel this process
        result = {p[0]: self._proceed_period(
            p[1:], group_by, filters=filters, limit=limit) for p in periods}

        resp.media = {'data': result}
        resp.status = falcon.HTTP_200

    def _proceed_period(self, period, group_by, *, filters=None, limit=None):
        date_from = period[0].replace(hour=0, minute=0, second=0)
        # Because period can contain only 1 date => get stats for 1 day
        date_to = period[-1].replace(hour=23, minute=59, second=59)
        self.logger.debug(
            f'Collecting stats overview from {date_from} to {date_to}')

        # Default aggregate pipeline
        query = [
            {
                '$match': {
                    'created_at': {'$gte': date_from, '$lt': date_to},
                    'revenue': {'$gt': 0},
                }
            },
            {
                '$group': {
                    '_id': None,
                    'revenue': {'$sum': '$revenue'},
                    'payout': {'$sum': '$payout'}
                }
            },
            {
                '$sort': {'revenue': -1}
            }
        ]

        # Filling pipeline with argument values
        query[1]['$group']['_id'] = {k: f'${k}' for k in group_by}
        if filters is not None:
            query[0]['$match'].update(filters)
        if limit is not None:
            query.append({'$limit': limit})

        # Checking cache
        query_key = 'stats:periods:' + self.cache.md5_hash(query)
        if (c := self.cache.get(query_key)) is not None:
            return c
        groups = self._get_mongo_period(group_by, query)

        if 'status' in group_by:
            # If grouped by `status` (`status` must be at last
            # last level of grouping tree), add `total` leaves
            self._postprocess_statuses(groups)

        self.cache.set(query_key, groups, expires_in=self.CACHE_TTL)
        return groups

    def _get_mongo_period(self, group_keys, query) -> dict:
        result = {}

        r = {}
        for i in self.mongo['conversions'].aggregate(query):
            # Building tuple key from grouping values
            gkeys = []
            for k in group_keys:
                if k == "publisher":
                    gkeys.append(i['_id'][k]['name'])
                elif k == "advertiser":
                    gkeys.append(i['_id'][k]['title'])
                elif k == "offer":
                    gkeys.append(f"({i['_id'][k]['id']}) {i['_id'][k]['title']} - {i['_id'][k].get('internal_id')}")
                else:
                    gkeys.append(i['_id'][k])

            # Creating new nodes if they don't exist in aggregation tree
            l = result
            for gkey in gkeys:
                if gkey not in l:
                    l[gkey] = {}
                l = l[gkey]

            # Creating final point
            for k in i:
                if k != '_id':
                    l[k] = round(i[k], 3)

        return result

    def _postprocess_statuses(self, groups):
        keys = list(groups.keys())
        for k in keys:
            if k in ['confirmed', 'declined']:
                if 'total' not in groups:
                    groups['total'] = {'revenue': 0, 'payout': 0}
                for metric in groups[k]:
                    groups['total'][metric] += groups[k][metric]

            if isinstance(groups[k], dict):
                self._postprocess_statuses(groups[k])
            else:
                return

    def _validate_date_param(self, req, key) -> datetime:
        """Returns datetime object parsed from string by date pattern

        Args:
            req (falcon.Request): Request object
            key (str): Name of query parameter where date is located

        Raises:
            falcon.HTTPBadRequest: If validation of query param failed
        """
        s = req.params.get(key)
        if s is None:
            raise falcon.HTTPBadRequest(f"'{key}' is required in query")
        return datetime.strptime(s, self.DATE_PATTERN)

    def _get_param_as_int(self, req, key):
        s = req.params.get(key)
        if s is not None:
            s = int(s)
        return s

    def _get_filters_from_query(self, req):
        """ Returns dict of filters parsed from query params named like
            `filter[``<any of class filters>``]`. If there are no filters, returns None
        """
        filters = self.DEFAULT_FILTERS.copy()
        for p in self.QUERY_FILTERS:
            if (f := req.params.get(f'filter[{p}]')) is not None:
                filters[f'{p}.id'] = f
                if p in ['publisher', 'offer']:
                    filters[f'{p}.id'] = int(f)
        return filters if len(filters) else None

    def _get_mongo_overcaps(self):

        def overcap_row(offer, cap, convs, pub=None):
            r = {
                'offer_id': offer['id'],
                'offer_name': offer['title'],
                'affise_id': offer['internal_id'],
                'advertiser': offer['advertiser']['title'],
                'cap': cap,
                'convs': convs,
            }
            if pub is not None:
                r['pub_id'] = pub['id']
                r['pub_name'] = pub['name']

            return r

        # Get yesterday's paid convs from Mongo grouped by offers
        grouped_convs = list(self.mongo['conversions'].aggregate([
            {
                '$match': {
                    'created_at': {
                        '$lt': datetime.today().replace(hour=0, minute=0, second=0),
                        '$gte': (datetime.today()-timedelta(days=1)).replace(hour=0, minute=0, second=0)
                    },
                    'revenue': {'$gt': 0},
                },
            },
            {
                '$group': {
                    '_id': {
                        'offer': '$offer', 'pub': '$publisher'
                    },
                    'convs': {'$sum': 1},
                }
            },
        ]))

        # Dict of offers which contain caps
        capped_offers = {o['id']: o for o in self.mongo['offers'].find(
            {'id': {'$in': [c['_id']['offer']['id'] for c in grouped_convs]}
             }) if len(o['pub_caps']) or o['all_caps']}

        offer_overcaps = []
        offer_convs = {}
        pub_overcaps = []

        for conv in grouped_convs:
            pub = conv['_id']['pub']
            oid = conv['_id']['offer']['id']
            if oid not in capped_offers:
                continue

            cap = capped_offers[oid]['pub_caps'].get(
                str(pub['id']), capped_offers[oid]['all_caps'])

            if conv['convs'] > cap * 0.8:
                pub_overcaps.append(overcap_row(
                    capped_offers[oid], pub=pub, cap=cap, convs=conv['convs']))
                self.logger.debug(f"Pub overcap - {pub_overcaps[-1]}")

            offer_convs[oid] = offer_convs.get(oid, 0) + conv['convs']

        for offer_id, convs in offer_convs.items():
            offer = capped_offers[offer_id]
            if offer['all_caps'] and convs > offer['all_caps'] * 0.8:
                offer_overcaps.append(overcap_row(
                    offer, cap=offer['all_caps'], convs=convs))
                self.logger.debug(f"Offer overcap - {offer_overcaps[-1]}")

        return {'offers': offer_overcaps, 'pubs': pub_overcaps}
