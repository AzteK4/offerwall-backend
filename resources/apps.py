import logging
import falcon
import json
import time
import math

from copy import deepcopy
from redis import StrictRedis, ConnectionPool
from datetime import datetime, timedelta

REDIS_HOST = 'localhost'
REDIS_PASS = 'ubff5c4y2d8WEWPJ'

class AppsResource():

    PAGE_LIMIT = 25
    STR_FILTERS = [
        'os', 'adv', 'app', 'tags', 'status', 'countries', 'app_genres']
    INT_FILTERS = ['offer_id', ]

    CACHE_TTL = 300
    REDIS_KEY_PATTERN = '{date}_paid_convs_count:{offer}_'

    def __init__(self, mongo_client, redis, cache):
        self.mongo = mongo_client
        self.cache = cache
        self.logger = logging.getLogger('AppsResource')

        self.redis = redis

    def on_get(self, req, resp):
        # Pagination parameters
        page = req.get_param_as_int('page') or 1
        limit = req.get_param_as_int('limit') or self.PAGE_LIMIT

        query = [
            {'$match': {'os': {'$ne': ''}}},
            {
                '$group': {
                    '_id': '$app_id',
                    'max_date': {'$max': '$created_at'},
                    'offers': {'$push': '$$ROOT'},
                }
            },
            {'$sort': {'max_date': -1}},
            {'$skip': (page-1)*limit},
            {'$limit': limit}
        ]

        # Filtering parameters
        for field, f in self._get_filters(req).items():
            query[0]['$match'][field] = f
        self.logger.debug(f'Apps query - {query}')

        # TODO: Checking cache?
        apps = [self._parse_app(a)
                for a in self.mongo['offers'].aggregate(query)]
        self.logger.info(f'Received apps ({len(apps)})')

        # # Filling conv history
        # all_offers = []
        # for app in apps:
        #     all_offers.extend(app['connected_offers'])

        # for offer in all_offers:
        #     self._load_redis_convs(offer)
        #     # dict -> list
        #     offer['connected_pubs'] = sorted(
        #         offer['connected_pubs'].values(), key=lambda k: k['id'])

        #     if offer['privacy'] == 'public':
        #         # Delete inactive publishers for public offers
        #         pubs = offer['connected_pubs']
        #         pubs = [p for p in pubs if sum(
        #             [day['count'] for day in p['conversions']]) > 0]

        resp.media = {
            'data': apps,
            'pagination': {
                'page': page,
                'limit': limit,
                'total': len(self.mongo['offers'].distinct('app_id', query[0]['$match'])),
            }
        }
        resp.status = falcon.HTTP_200

    def on_get_pubs(self, req, resp, offer_id):
        """Returns array of descending dates with count of paid convs per day
        """
        day = datetime.today()
        zero_convs = [{'date': (
            day - timedelta(days=i)).strftime('%Y-%m-%d'), 'count': 0} for i in range(8)]

        pub_dict = {}
        mongo_offer = self.mongo['offers'].find_one({'id': int(offer_id)})

        # Insert active pubs for offer
        for pub in mongo_offer.get('connected_pubs', []):
            pub_dict[pub['id']] = {
                'id': pub['id'],
                'title': pub['name'],
                'is_active': True,
                'convs': deepcopy(zero_convs)
            }

        for i in range(8):
            redis_pubs = [key.split('_')[-1] for key in self.redis.keys(self.REDIS_KEY_PATTERN.format(
                date=day.strftime('%Y%m%d'), offer=offer_id) + '*')]

            for redis_pub in redis_pubs:
                convs = int(self.redis.get(self.REDIS_KEY_PATTERN.format(
                    date=day.strftime('%Y%m%d'), offer=offer_id) + redis_pub))

                redis_pub = int(redis_pub)
                if redis_pub not in pub_dict:
                    # Create new non-active pub
                    pub_dict[redis_pub] = {
                        'id': int(redis_pub),
                        'is_active': False,
                        'convs': deepcopy(zero_convs)
                    }
                pub_dict[redis_pub]['convs'][i]['count'] = convs

            day -= timedelta(days=i)

        resp.media = {
            'data': sorted([p for p in pub_dict.values()], key=lambda p: p['id']),
        }
        resp.status = falcon.HTTP_200

    def _get_filters(self, req):
        filters = {}
        match = {}

        # Checking for existance
        for f in self.STR_FILTERS:
            filters[f] = req.get_param(f) or ''
        for f in self.INT_FILTERS:
            filters[f] = req.get_param_as_int(f) or 0

        # Equality filters
        for f in ['os', 'status', 'offer_id']:
            if filters[f]:
                match['id' if f == 'offer_id' else f] = {'$eq': filters[f]}

        # List filters
        if filters['adv']:
            match['advertiser.id'] = {'$in': filters['adv'].split(',')}

        if filters['countries']:
            match['countries'] = {
                '$in': [i.upper() for i in filters['countries'].split(',')]}

        if filters['app_genres']:
            match['application.genres'] = {
                '$in': filters['app_genres'].split(',')}

        if filters['tags']:
            match['tags'] = {'$in': filters['tags'].split(',')}

        # Text filters
        if filters['app']:
            match['$text'] = {'$search': ' '.join([
                f'\"{w}\"' for w in filters['app'].split(" ")])}

        return match

    def _parse_app(self, app):
        a = app['offers'][0]['application']

        new_app = {
            'app_id': app['_id'],
            'app_name': a.get('name', ''),
            'os': app['offers'][0]['os'],
            'genres': a.get('genres', []),
            'reviews_rate': a.get('reviews_rate', None),
            'reviews_count': a.get('reviews_count') or 0,

            'connected_offers': [],
        }

        if new_app['reviews_count'] > 0:
            new_app['reviews_count'] = prettify_number(new_app['reviews_count'])

        # # # Create historical convs
        # day = datetime.now()
        # zero_convs = [{'date': (
        #     day - timedelta(days=i)).strftime('%Y-%m-%d'), 'count': 0} for i in range(8)]

        for offer in app['offers']:
            o = {
                'offer_id': offer['id'],
                'offer_name': offer['title'],
                'internal_id': offer['internal_id'],
                'adv': offer['advertiser']['title'],

                'countries': offer['countries'],
                'tags': offer.get('tags', []),
                'privacy': offer['privacy'],
                'status': offer['status'],
                'created_at': offer['created_at'],

                'payout': offer['payout'],
                'revenue': offer['revenue'],

                'daily_cap': offer.get('all_caps', 0),
                'pub_caps': offer['pub_caps'],

                'connected_pubs': []
            }

            if not o['daily_cap'] and len(offer['pub_caps']):
                new_app['daily_cap'] = sum(offer['pub_caps'].values())

            o['conversions'] = self._get_offer_convs(o['offer_id'])
            new_app['connected_offers'].append(o)

        new_app['connected_offers'] = sorted(new_app['connected_offers'],
                                             key=lambda k: k['offer_id'], reverse=True)
        if new_app['app_name'] == '':
            new_app['app_name'] = new_app['connected_offers'][0]['offer_name']
            for offer in new_app['connected_offers'][1:]:
                new_app['app_name'] = get_substring(new_app['app_name'], offer['offer_name'])

        return new_app

    # def _load_redis_convs(self, offer):
    #     # Create historical convs
    #     day = datetime.now()
    #     zero_convs = [{'date': (
    #         day - timedelta(days=i)).strftime('%Y-%m-%d'), 'count': 0} for i in range(8)]

    #     for i in range(8):
    #         for pub in self._get_redis_pubs(offer['offer_id'], day):
    #             if pub not in offer['connected_pubs']:
    #                 offer['connected_pubs'][pub] = {
    #                     'id': pub,
    #                     'is_active': False,
    #                     'conversions': deepcopy(zero_convs),
    #                 }

    #             convs = self._get_redis_convs(offer['offer_id'], pub, day)
    #             offer['connected_pubs'][pub]['conversions'][i]['count'] = int(convs)

    #         day -= timedelta(days=1)

    def _get_offer_convs(self, offer_id):
        day = datetime.today()
        convs = []
        for i in range(8):
            pubs = self.redis.keys(self.REDIS_KEY_PATTERN.format(
                date=day.strftime('%Y%m%d'), offer=offer_id) + '*')

            convs.append({'date': day.strftime('%Y-%m-%d')})
            if len(pubs):
                convs[-1]['count'] = sum([int(self.redis.get(s)) for s in pubs])
            else:
                convs[-1]['count'] = 0
            day -= timedelta(days=1)

        return convs

    # def _get_redis_convs(self, offer_id, pub_id, day=datetime.now()):
    #     return self.redis.get(self.REDIS_KEY_PATTERN.format(
    #         date=day.strftime('%Y%m%d'), offer=offer_id) + str(pub_id))

    # def _get_redis_pubs(self, offer_id, day=datetime.now()):
    #     pubs = self.redis.keys(self.REDIS_KEY_PATTERN.format(
    #         date=day.strftime('%Y%m%d'), offer=offer_id) + '*')
    #     return [int(pub.split('_')[-1]) for pub in pubs]

# Not class methods

def get_substring(s1, s2):
    m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]

def prettify_number(n, precision=0):
    names = ['', 'k', 'M']
    n = float(n)

    millidx = max(0, min(len(names)-1, int(math.floor(0 if n == 0 else math.log10(abs(n)) / 3))))
    result = '{:.{precision}f}'.format(n / 10**(3 * millidx), precision=precision)
    return '{0}{dx}'.format(result, dx=names[millidx])
