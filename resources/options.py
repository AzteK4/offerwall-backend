import json
import falcon
import logging


class OptionResourse:
    CACHE_TTL = 300
    CACHE_KEY = 'options:options'

    def __init__(self, mongo_client, cache):
        self.mongo = mongo_client
        self.cache = cache

    def on_get(self, req, resp):
        resp.media = {'data': self._get_or_cache()}
        resp.status = falcon.HTTP_200

    def _get_or_cache(self):
        r = self.cache.get(self.CACHE_KEY)
        if r is not None:
            return json.loads(r)

        r = self._get_resourse()
        self.cache.set(self.CACHE_KEY, json.dumps(r), expires_in=self.CACHE_TTL)
        return r

    def _get_resourse(self):
        raise NotImplementedError


class AppGenresResource(OptionResourse):
    CACHE_TTL = 3600
    CACHE_KEY = 'options:genres'

    def _get_resourse(self):
        return sorted(
            [{'name': genre}
                for genre in self.mongo['applications'].distinct("genres")],
            key=lambda k: k['name'])


class CountriesResource(OptionResourse):
    CACHE_TTL = 3600
    CACHE_KEY = 'options:countries'

    def _get_resourse(self):
        return sorted([{
            'name': i['name'], 'code': i['code'],
            'checker_enabled': i.get('checker_enabled', False),
        } for i in self.mongo['countries'].find()], key=lambda k: k['name'])


class AdvertisersResource(OptionResourse):
    CACHE_TTL = 300
    CACHE_KEY = 'options:advs'

    def _get_resourse(self):
        return sorted(
            [{'title': i['title'], 'id': i['id']}
                for i in self.mongo['advertisers'].find()],
            key=lambda k: k['title'])


class PublishersResource(OptionResourse):
    CACHE_TTL = 300
    CACHE_KEY = 'options:pubs'

    def _get_resourse(self):
        r = self.mongo['publishers'].find({'status': 'active'})
        return sorted([{
            'title': i['name'], 'id': i['id']
        } for i in r], key=lambda k: k['title'])
