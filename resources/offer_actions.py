import falcon
import os

from .affise_api import API

class OfferActionsResource():
    def __init__(self, mongo_client, url, api_key):
        self.mongo = mongo_client
        self.affise = API(url, api_key)

    def on_get_enable_pub(self, req, resp, offer_id):
        pub_id = req.get_param_as_int('pub_id')
        new_cap = req.get_param_as_int('cap')

        r = self.affise.ns('offer').mass_edit('enable-affiliate', data={
            'offer_id': offer_id, 'pid': pub_id,
        })

        if r['status'] == 1:
            pub = self.mongo['publishers'].find_one({"id": pub_id})
            self.mongo['offers'].update_one({"id": int(offer_id)},
                {"$addToSet": {"connected_pubs": {"id": pub_id, 'name': pub['name']}}}
            )

        if new_cap is not None:
            affise_caps = self._import_affise_caps(offer_id)

            cap_exists = False
            for cap in [c for c in affise_caps if c['affiliate_type'] == 'exact']:
                if pub_id in cap['affiliates[]']:
                    cap['value'] = new_cap
                    cap_exists = True

            if not cap_exists:
                affise_caps.append({
                    'period': 'day', 'goal_type': 'exact', 'goals[]': ['1'], 'type': 'conversions',
                    'value': new_cap, 'affiliate_type': 'exact', 'affiliates[]': pub_id,
                })

            result = affise.ns('admin').edit('offer', offer_id, affise.make_array_struct(name='caps', lst=affise_caps))
            if result['status'] == 1:
                self.mongo['offers'].update_one({'id': int(offer_id)}, {'$set': {'pub_caps.%s' % pub_id: new_cap}})
            else:
                r['message'] += '. %s' % r['message']

        resp.media = r
        resp.status = falcon.HTTP_200


    def on_get_disable_pub(self, req, resp, offer_id):
        pub_id = req.get_param_as_int('pub_id')

        r = self.affise.ns('offer').mass_edit('disable-affiliate', data={
            'offer_id': offer_id, 'pid': pub_id,
        })

        if r['status'] == 1:
            self.mongo['offers'].update_one({"id": int(offer_id)},
                {"$pull": {"connected_pubs": {"id": pub_id}}}
            )

        resp.media = r
        resp.status = falcon.HTTP_200

    def on_get_change_caps(self, req, resp, offer_id):
        pub_id = req.get_param_as_int('pub_id')
        new_cap = req.get_param_as_int('new_cap')

        affise_caps = self._import_affise_caps(offer_id)

        cap_exists = False
        for cap in [c for c in affise_caps if c['affiliate_type'] == 'exact']:
            if pub_id in cap['affiliates[]']:
                cap['value'] = new_cap
                cap_exists = True

        if not cap_exists:
            affise_caps.append({
                'period': 'day', 'goal_type': 'exact', 'goals[]': ['1'], 'type': 'conversions',
                'value': new_cap, 'affiliate_type': 'exact', 'affiliates[]': pub_id,
            })

        r = self.affise.ns('admin').edit('offer', offer_id, self.affise.make_array_struct(name='caps', lst=affise_caps))
        if r['status'] == 1:
            result = self.mongo['offers'].update_one({'id': int(offer_id)}, {'$set': {"pub_caps.%s" % pub_id: new_cap}})
            del r['offer']
            r['message'] = 'Cap was changed successfully'

        resp.media = r
        resp.status = falcon.HTTP_200

    def _import_affise_caps(self, offer_id):
        affise_offer = self.affise.get('offer', offer_id)
        caps = []

        for cap in affise_offer.get('caps', []):
            if cap['affiliate_type'] == 'all' or cap['affiliate_type'] == 'each':
                caps.append({
                    'period': cap['period'], 'goal_type': cap['goal_type'], 'type': cap['type'],
                    'goals[]': list(cap['goals'].keys()),
                    'value': cap['value'], 'affiliate_type': cap['affiliate_type'],
                })
            if cap['affiliate_type'] == 'exact':
                caps.append({
                    'period': cap['period'], 'goal_type': cap['goal_type'], 'type': cap['type'],
                    'goals[]': list(cap['goals'].keys()), 'affiliates[]': cap['affiliates'],
                    'value': cap['value'], 'affiliate_type': 'exact',
                })

        return caps