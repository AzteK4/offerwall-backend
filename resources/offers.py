import logging
import falcon
import json
import time
import math

from copy import deepcopy
from redis import StrictRedis, ConnectionPool
from datetime import datetime, timedelta

REDIS_HOST = 'localhost'
REDIS_PASS = 'ubff5c4y2d8WEWPJ'

class OffersResource():

    # PAGE_LIMIT = 25
    CACHE_TTL = 300
    REDIS_KEY_PATTERN = '{date}_paid_convs_{metric}:{offer}_'

    def __init__(self, mongo_client, redis, cache):
        self.mongo = mongo_client
        self.cache = cache
        self.logger = logging.getLogger('OffersResource')

        self.redis = redis

    def on_get_cpa_offers(self, req, resp):
        # Three months before start of the month
        day = datetime.now().replace(day=1, hour=0, minute=0, second=0)
        day -= timedelta(days=90)

        query = [
            {
                '$match': {
                    'goal': {'$ne': '1'},
                    'created_at': {'$gte': day}
                }
            },
            {
                '$group': {'_id': {
                    'offer': '$offer',
                    'advertiser': '$advertiser'
                }}
            }
        ]

        # for m_offer in self.mongo['conversions'].aggregate(query):


        # r = list(self.mongo['conversions'].aggregate(query))
        # print (len(r))
        # print (r[0])
