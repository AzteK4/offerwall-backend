import falcon
import logging

from pymongo import MongoClient
from redis import ConnectionPool, StrictRedis

import secret

from middlewares import *
from resources.stats import StatsResource
from resources.options import *
from resources.apps import AppsResource
from resources.offer_actions import OfferActionsResource
from resources.offers import OffersResource

from utils.color_logging import ColoredLogger
from utils.cache_manager import MemcacheManager


logging.setLoggerClass(ColoredLogger)

mongo_client = MongoClient(secret.MONGO_URI)['offerwall']
cache = MemcacheManager(host=secret.MEMCACHE_HOST, port='11211')

pool = ConnectionPool(
    host=secret.REDIS_HOST, password=secret.REDIS_PASS, db=1, decode_responses=True)
redis = StrictRedis(connection_pool=pool)

api = falcon.API(middleware=[
    CORSMiddleware(),
    LoggingMiddleware(),
    AuthMiddleware(cache=cache, exempt_routes=[
        '/auth/login',
    ]),
])

stats = StatsResource(mongo_client, cache)
api.add_route('/stats/periods', stats, suffix='periods')
api.add_route('/stats/comparison', stats, suffix='comparison')
api.add_route('/stats/groupby', stats, suffix='groupby')
api.add_route('/stats/overcaps', stats, suffix='overcaps')

################## Options ##################

countries = CountriesResource(mongo_client, cache)
api.add_route('/countries', countries)

advertisers = AdvertisersResource(mongo_client, cache)
api.add_route('/advertisers', advertisers)

publishers = PublishersResource(mongo_client, cache)
api.add_route('/publishers', publishers)

app_genres = AppGenresResource(mongo_client, cache)
api.add_route('/genres', app_genres)

#################### Apps ####################

apps = AppsResource(mongo_client, redis, cache)
api.add_route('/apps', apps)
api.add_route('/offer/{offer_id}/pubs', apps, suffix='pubs')

################# Management #################

offer_actions = OfferActionsResource(mongo_client, secret.AFFISE_API, secret.AFFISE_KEY)
api.add_route('/offer/{offer_id}/enable_pub',
              offer_actions, suffix='enable_pub')
api.add_route('/offer/{offer_id}/disable_pub',
              offer_actions, suffix='disable_pub')
api.add_route('/offer/{offer_id}/change_caps',
              offer_actions, suffix='change_caps')

offers = OffersResource(mongo_client, redis, cache)
api.add_route('/cpa_offers', offers, suffix='cpa_offers')