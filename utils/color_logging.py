import logging

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
COLORS = {
    'WARNING': YELLOW,
    'INFO': WHITE,
    'DEBUG': BLUE,
    'CRITICAL': YELLOW,
    'ERROR': RED
}

RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[1;%dm"
BOLD_SEQ = "\033[1m"


def formatter_message(message, use_color=True):
    if use_color:
        message = message.replace(
            "$RESET", RESET_SEQ).replace("$BOLD", BOLD_SEQ)
    else:
        message = message.replace("$RESET", "").replace("$BOLD", "")
    return message


class ColoredFormatter(logging.Formatter):
    def __init__(self, msg, use_color=True):
        logging.Formatter.__init__(self, msg)
        self.use_color = use_color

    def format(self, record):
        levelname = record.levelname
        if self.use_color and levelname in COLORS:
            # The background is set with 40 plus the number of the color, and the foreground with 30
            levelname_color = COLOR_SEQ % (
                30 + COLORS[levelname]) + levelname + RESET_SEQ
            record.levelname = levelname_color
        return logging.Formatter.format(self, record)


# Custom logger class with multiple destinations
class ColoredLogger(logging.Logger):
    FORMAT = '$BOLD%(asctime)s$RESET - %(levelname)s - %(name)s - %(funcName)s - %(message)s'
    NOCOLOR_FORMAT = formatter_message(FORMAT, False)

    def __init__(self, name):
        logging.Logger.__init__(self, name, logging.DEBUG)

        color_formatter = ColoredFormatter(formatter_message(self.FORMAT))
        nocolor_formatter = ColoredFormatter(
            self.NOCOLOR_FORMAT, use_color=False)

        # Output full log
        fh = logging.FileHandler('logs/access.log')
        fh.setFormatter(nocolor_formatter)
        self.addHandler(fh)

        # Output for errors & warnings
        fh = logging.FileHandler('logs/errors.log')
        fh.setLevel(logging.WARNING)
        fh.setFormatter(nocolor_formatter)
        self.addHandler(fh)

        console = logging.StreamHandler()
        console.setFormatter(color_formatter)
        self.addHandler(console)

        return
