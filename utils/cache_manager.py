import logging
import hashlib
import json
import memcache

from datetime import datetime, date

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.strftime('%Y-%m-%d %H:%M:%S')
    raise TypeError ("Type %s not serializable" % type(obj))

class CacheManager:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def get(self, key):
        raise NotImplementedError

    def set(self, key, value, expires_in=None, expires_at=None) -> bool:
        raise NotImplementedError


class MemcacheManager(CacheManager):
    def __init__(self, host, port):
        super().__init__()
        self.cache = memcache.Client((host, port))

    def get(self, key):
        return self.cache.get(key)

    def set(self, key, value, expires_in=None, expires_at=None) -> bool:
        ttl = 0
        if expires_in is not None:
            ttl = expires_in
        elif expires_at is not None:
            # expires_in is None && expires_at is not None
            ttl = int(expires_at - datetime.now().timestamp())

        self.logger.info(f"Saving key '{key}' in cache with TTL={ttl}")
        r = self.cache.set(key, value, time=ttl)
        return True
        # return self.cache.set(key, value, time=ttl)

    def md5_hash(self, obj) -> str:
        # Because datetime can't be JSON serialized by default
        obj_str = json.dumps(obj, default=json_serial)

        m = hashlib.md5()
        m.update(obj_str.encode())
        return m.hexdigest()